import React from 'react';
import ReactDOM from 'react-dom';

import {List, Map} from 'immutable';
import {compose, createStore} from 'redux';
import {Provider} from 'react-redux';

import {reducer, save_todos} from './reducer';

import {TodoApp, TodoAppContainer} from './components/TodoApp';
import moment from 'moment';


const createStoreDevTools = compose(
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);
const store = createStoreDevTools(reducer);

store.dispatch({
  type: 'SET_STATE',
  state: {
    todos: [
      {id: 1, text: 'React', status: 'active', editing: false, startDate: moment()},
      {id: 2, text: 'Redux', status: 'active', editing: false, startDate: moment()},
      {id: 3, text: 'Immutable', status: 'completed', editing: false, startDate: moment()},
    ],
    filter: 'all'
  }
});

function handleChange(){
  console.log("handleChange");
  save_todos(store.getState());
}

store.subscribe(handleChange);

require('../node_modules/todomvc-app-css/index.css');
require('../node_modules/react-datepicker/dist/react-datepicker.css');
require('./main.css');


ReactDOM.render(
  <Provider store={store}>
    <TodoAppContainer/>
  </Provider>,
  document.getElementById('app')
);
