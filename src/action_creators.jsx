export function toggleComplete(itemId) {
  console.log("toggleComplete de action_creators" + itemId);
  return {
    type: 'TOGGLE_COMPLETE',
    itemId
  }
}

export function changeFilter(filter) {
  return {
    type: 'CHANGE_FILTER',
    filter
  }
}

export function editItem(itemId) {
  console.log(itemId);
  return {
    type: 'EDIT_ITEM',
    itemId
  }
}

export function cancelEditing(itemId) {
  return {
    type: 'CANCEL_EDITING',
    itemId
  }
}

export function doneEditing(itemId, newText) {
  return {
    type: 'DONE_EDITING',
    itemId,
    newText
  }
}

export function addItem(newText) {
  console.log("adding item" + newText);
  return {
    type: 'ADD_ITEM',
    newText
  }
}

export function deleteItem(itemId) {
    console.log("deleteItem de action_creators" + itemId);
  return {
    type: 'DELETE_ITEM',
    itemId
  }
}

export function clearCompleted() {
  return {
    type: 'CLEAR_COMPLETED'
  }
}

export function setDate(itemId, newDate) {
  return {
    type: 'SET_DATE',
    itemId,
    newDate
  }
}
