import {Map} from 'immutable';
import moment from 'moment';

import './components/modernizr.js';
import './components/json2.js';


export function save_todos(data) {
  console.log("Local stortage" + Modernizr.localStorage);
  if (window.localStorage) {
    var localStorage = window.localStorage;
    localStorage["madata"] = JSON.stringify(data);
    console.log("enregistrement : " + JSON.stringify(data));
  }
  var toto = JSON.stringify(data);
  console.log(data);
}

function restore_todos(defaultValue) {
  if (window.localStorage) {
    var localStorage = window.localStorage;
    if (localStorage["madata"] === null ||
        localStorage["madata"] === undefined){
          return defaultValue;
        }
    else {
      return JSON.parse(localStorage["madata"], (k,v) => {
        if (k === 'startDate'){
          return moment(v);
        }
        else {
          return v;
        }
      });
    }
  }
  else {
    return defaultValue;
  }
}

function setState(state, newState) {
  var restore = restore_todos(newState);
  if (restore !== newState){
    console.log("different");
    newState = restore;
  }
  return state.merge(newState);
}

export function reducer(state = Map(), action) {
  switch (action.type) {
    case 'SET_STATE':
      return setState(state, action.state);
    case 'TOGGLE_COMPLETE':
      return toggleComplete(state, action.itemId);
    case 'CHANGE_FILTER':
      return changeFilter(state, action.filter);
    case 'EDIT_ITEM':
      return editItem(state, action.itemId);
    case 'CANCEL_EDITING':
      return cancelEditing(state, action.itemId);
    case 'DONE_EDITING':
      return doneEditing(state, action.itemId, action.newText);
    case 'ADD_ITEM':
      return addItem(state, action.newText);
    case 'DELETE_ITEM':
      return deleteItem(state, action.itemId);
    case 'CLEAR_COMPLETED':
      return clearCompleted(state);
    case 'SET_DATE':
      return setDate(state, action.itemId, action.newDate);
  }
  return state;
}

function findItemIndex(state, itemId) {
  return state.get('todos').findIndex(
    (item) => item.get('id') === itemId
  );
}

function toggleComplete(state, itemId) {
  console.log("toggleComplete de reducer");
  const itemIndex = findItemIndex(state, itemId);

  const updatedItem = state.get('todos').get(itemIndex).update('status',
    status => status === 'active' ? 'completed' : 'active');

    return state.update('todos', todos => todos.set(itemIndex, updatedItem));
}


function editItem(state, itemId) {
  console.log("item Id : "+itemId);
  const itemIndex = findItemIndex(state, itemId);
  console.log("itemIndex : "+itemIndex);

  const updatedItem = state.get('todos')
    .get(itemIndex)
    .set('editing', true);

    return state.update('todos', todos => todos.set(itemIndex, updatedItem));
}

function cancelEditing(state, itemId) {
  const itemIndex = findItemIndex(state, itemId);
  const updatedItem = state.get('todos')
    .get(itemIndex)
    .set('editing', false);

    return state.update('todos', todos => todos.set(itemIndex, updatedItem));
}

function doneEditing(state, itemId, newText) {
  const itemIndex = findItemIndex(state, itemId);
  const updatedItem = state.get('todos')
    .get(itemIndex)
    .set('editing', false)
    .set('text', newText);

    return state.update('todos', todos => todos.set(itemIndex, updatedItem));
}

function changeFilter(state, filter) {
  return state.set('filter', filter);
}

function addItem(state, newText) {
  const todolist = state.get('todos');
  const newId = state.get('todos').size + 1;
  console.log('new id:'+newId);
  const newItem = Map({id: newId, status: 'active', text: newText, editing: false, startDate: moment()});


  return state.update('todos', todos => todos.push(newItem));
}


function deleteItem(state, itemId) {
  const itemIndex = findItemIndex(state, itemId);
  return state.update('todos', todos => todos.remove(itemIndex));
}

function clearCompleted(state) {
  return state.update('todos', todos => todos.filter(
    (item) => item.get('status') !== 'completed'
  ));
}

function setDate(state, itemId, newDate) {
  console.log("new date "+newDate);
  const itemIndex = findItemIndex(state, itemId);
  const updatedItem = state.get('todos')
    .get(itemIndex)
    .set('startDate', newDate);

    return state.update('todos', todos => todos.set(itemIndex, updatedItem));
}
