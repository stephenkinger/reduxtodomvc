import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

import ReactDOM from 'react-dom'

export default class TextInput extends React.Component {
  constructor(props) {
    super(props);
//    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    console.log("valeur recue : "+this.props.text);
    this.state = {'value': this.props.text}
  }
  cancelEditing() {
    this.setState({'value': this.props.text});
    return this.props.cancelEditing(this.props.itemId);
  }
  _handleKeyDown(e) {
    switch (e.key) {
      case 'Enter':
        return this.props.doneEditing(this.props.itemId, this.state.value);
      case 'Escape':
        return this.cancelEditing();
    }
  }
  _handleOnBlur(e) {
    return this.cancelEditing();
  }
  _handleOnChange(e) {
    this.setState({value: e.target.value});
  }
  render() {
    console.log("Affichage Text Input " + this.props.text);
    return <input className="edit"
                  type="text"
                  autoFocus
                  value={this.state.value}
                  onChange={this._handleOnChange.bind(this)}
                  ref={ function(component) {
                    if (component != null) {
                      ReactDOM.findDOMNode(component).focus();
                    }
                  }
                  }
                  onKeyDown={this._handleKeyDown.bind(this)}
                  onBlur={this._handleOnBlur.bind(this)} />
  }

};
