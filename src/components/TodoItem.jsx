import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import TextInput from './TextInput';
import classNames from 'classnames';
import DatePicker from 'react-datepicker';




export default class TodoItem extends React.Component {
//  constructor(props) {
//    super(props);
//    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
//    this.state = {
//      startDate: moment()
//    }
//  }

  handleOnChange(date) {
    //this.setState({startDate: date});
    this.props.setDate(this.props.id, date);
  }

  render() {
    var itemClass = classNames({
      'todo': true,
      'completed': this.props.isCompleted,
      'editing': this.props.isEditing,
      'startDate': this.props.startDate
    });
    console.log("startDate "+this.props.startDate);
    return <li className={itemClass}>
      <div className="view">
        <input type="checkbox"
               className="toggle"
               defaultChecked={this.props.isCompleted}
               onClick={()=> this.props.toggleComplete(this.props.id)}/>
        <label htmlFor="todo"
               ref="text"
                onDoubleClick={()=>this.props.editItem(this.props.id)}>
                {this.props.text}
        </label>
        <DatePicker className="picker"
                    dateFormat="DD/MM/YYYY"
                    selected={this.props.startDate}
                    onChange={this.handleOnChange.bind(this)}
                    />
        <button className="destroy"
                onClick={()=>this.props.deleteItem(this.props.id)}></button>
      </div>
      <TextInput text={this.props.text}
                  itemId={this.props.id}
                  cancelEditing={this.props.cancelEditing}
                  doneEditing={this.props.doneEditing}/>
    </li>
  }
};
